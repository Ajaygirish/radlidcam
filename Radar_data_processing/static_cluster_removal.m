% Radar .csv file data processing to separate static clusters 
% import any .csv file and name it csv

for i=1:size(csv,1) %for loop to replace first column with index numbers
csv(i,1)=i;
end
idx_0=find(csv(:,5)==0); %finding clusters with velocity zero
csv(idx_0,:)=[];
x=csv(:,3);
y=csv(:,4);
figure(1);
plot(x,y,'bo') %plots clusters with zero velocity alone


idx_rcs=find(csv(:,7)>15&csv(:,7)<25); %finding clusters (car) with rcs btw 15dBsm and 25dBsm
csv_1=csv;                             %variable for downsampled .csv file  
csv_1=csv_1(idx_rcs,:);
x_1=csv_1(:,3);
y_1=csv_1(:,4);
figure(2)
plot(x_1,y_1,'go'); % plots cluster with RCS btw (15,25)dBsm and zero velocity
