import rospy
import pandas as pd
from visualization_msgs.msg import Marker

path = "/home/sujan/Downloads/2019-11-13_12_59_48_Car_vert_florian.csv"

df = pd.read_csv(path)

# print(df.values)
# print(df)

def talker():
    pub = rospy.Publisher('/visualization_marker_2', Marker, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    i = 0
    mark = Marker()
    while not rospy.is_shutdown():
        # fill these values before running the code and verfying that all values are in correct locations.
        #header
        mark.header.seq = 1 # give same value as id or someother increamenting number.
        mark.header.stamp.secs =  1 ### add add secs here
        mark.header.stamp.nsecs =  1### add nsecs here
        mark.header.frame_id = "/radar"

        mark.ns = "markers"
        mark.id = df.values[i, 1] # column 1
        mark.type = 2

        # position and orientation
        mark.pose.position.x = df.values[i, 2]  # column 2
        mark.pose.position.y = df.values[i, 3] # column 3 # check if this should be negative or positive
        mark.pose.position.z = 0

        mark.pose.orientation.x = 0
        mark.pose.orientation.y = 0
        mark.pose.orientation.z = 0
        mark.pose.orientation.w = 1

        #scale
        mark.scale.x = 0.2
        mark.scale.y = 0.2
        mark.scale.z = 0.2

        mark.color.r = 1
        mark.color.a = 1
        rospy.loginfo(mark)
        pub.publish(mark)
        rate.sleep()
        i+=1


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass