README : 
1.	This ROS-package is used for LIDAR data processing, Object detection and Classification
2.	Pre-requisites
a.	Ubuntu 14
b.	ROS Kinetic
c.	Python 2
d.	C++ 14 
3.	Python Packages to be installed
a.	Pcl
b.	Numpy
c.	Ros_numpy
d.	Scipy
e.	Pandas
4.	Installation steps
a.	cd ~/catkin_ws
b.	catkin init //if not initialized
c.	mkdir src //if not present
d.	cd src
e.	// copy the velodyne_obj_detection folder from https://devranbb@bitbucket.org/Ajaygirish/radlidcam
f.	cd ..
g.	catkin build //use �catkin make� if previously built using make
h.	source devel/setup.bash
i.	roslaunch velodyne_obj_detection velodyne_python.launch
