#!/usr/bin/env python

import rospy
import pcl
from sensor_msgs.msg import PointCloud2
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Point
import sensor_msgs.point_cloud2 as pc2
import numpy as np
import ros_numpy
from scipy.spatial import ConvexHull
import bbox
import dynamic_reconfigure.client
import copy
import math

mark = Marker()
mark.header.frame_id = "velodyne"
mark.id = 1
mark.type = mark.LINE_LIST
mark.action = mark.ADD
mark.scale.x = 0.03
mark.color.a = 1.0
mark.color.r = 1.0
mark.color.g = 1.0
mark.color.b = 0.0
mark.points = []

text_array = MarkerArray()


del_marker_array = MarkerArray()
del_marker = Marker()
del_marker.action = del_marker.DELETEALL
del_marker.header.frame_id = "velodyne"
del_marker_array.markers = [del_marker]

        
def parameter_callback(config):
    global clust_tol, min_size, max_size
    clust_tol = config.cluster_tolerance
    min_size = config.minimum_cluster_size
    max_size = config.maximum_cluster_size

def ros_to_pcl(ros_cloud):
    points_list = []
    for data in pc2.read_points(ros_cloud, skip_nans=True):
        points_list.append([data[0], data[1], data[2]])
    pcl_data = pcl.PointCloud()
    pcl_data.from_list(points_list)
    return pcl_data
    
def pcl_to_ros(pcl_cloud):
    ros_msg = ros_numpy.msgify(PointCloud2, np.array([(f[0], f[1], f[2], f[3]) for f in np.array(pcl_cloud.to_list())], dtype=dt))
    ros_msg.header.stamp = rospy.Time.now()
    ros_msg.header.frame_id = "velodyne"
    return ros_msg
    
def clustering(pcl_cloud):
    tree = pcl_cloud.make_kdtree()
    ec = pcl_cloud.make_EuclideanClusterExtraction()
    ec.set_ClusterTolerance(clust_tol)
    ec.set_MinClusterSize(min_size)
    ec.set_MaxClusterSize(max_size)
    ec.set_SearchMethod(tree)
    cluster_indices = ec.Extract()
    mark.points = []
    text_array.markers = []
    i = 0
    for cluster in cluster_indices:
        points = [pcl_cloud[point_indices][0:3] for point_indices in cluster]
        hull_ordered = np.array([points[index] for index in ConvexHull(points).vertices])
        np.append(hull_ordered, hull_ordered[0])        
        bb = bbox.minBoundingRect(hull_ordered[:,:-1])
        m_n = min(hull_ordered[:,-1])
        m_x = max(hull_ordered[:,-1])
	if m_x - m_n > 0.5:
		#text_temp = copy.copy(text)
		text = Marker()
		text.header.frame_id = "velodyne"
		text.scale.z = 0.7
		text.id = i
		text.type = text.TEXT_VIEW_FACING
		text.action = text.ADD
		text.color.r = 1
		text.color.g = 1
		text.color.b = 0.2
		text.color.a = 1
		area = math.sqrt((bb[0,0]-bb[1,0])**2 + (bb[0,1]-bb[1,1])**2)*math.sqrt((bb[2,0]-bb[1,0])**2 + (bb[2,1]-bb[1,1])**2)*(m_x - m_n)
		if area > 22:
			text.text = "Truck"
		elif area < 2.5:
			text.text = "Pedestrian"
		else:
			text.text = "Car"
		i = i+1
		text.pose.position.x = (bb[0,0] + bb[2,0])/2
		text.pose.position.y = (bb[0,1] + bb[2,1])/2
		text.pose.position.z = m_x + 0.3
		text_array.markers.append(text)
        	mark.points = mark.points + [Point(bb[0,0], bb[0,1], m_n), Point(bb[1,0], bb[1,1], m_n),
                        	Point(bb[2,0], bb[2,1], m_n), Point(bb[1,0], bb[1,1], m_n),
                        	Point(bb[2,0], bb[2,1], m_n), Point(bb[3,0], bb[3,1], m_n),
                        	Point(bb[0,0], bb[0,1], m_n), Point(bb[3,0], bb[3,1], m_n),
                        	Point(bb[0,0], bb[0,1], m_n), Point(bb[0,0], bb[0,1], m_x),
                        	Point(bb[1,0], bb[1,1], m_n), Point(bb[1,0], bb[1,1], m_x),
                        	Point(bb[2,0], bb[2,1], m_n), Point(bb[2,0], bb[2,1], m_x),
                        	Point(bb[3,0], bb[3,1], m_n), Point(bb[3,0], bb[3,1], m_x),
                        	Point(bb[0,0], bb[0,1], m_x), Point(bb[1,0], bb[1,1], m_x),
                        	Point(bb[2,0], bb[2,1], m_x), Point(bb[1,0], bb[1,1], m_x),
                        	Point(bb[2,0], bb[2,1], m_x), Point(bb[3,0], bb[3,1], m_x),
                        	Point(bb[0,0], bb[0,1], m_x), Point(bb[3,0], bb[3,1], m_x)]
    txt_marker.publish(del_marker_array)
    txt_marker.publish(text_array)
    pub_marker.publish(mark)
    
def callback(msg):
    pcl_cloud = ros_to_pcl(msg)
    clustering(pcl_cloud)
    

clust_tol = 0.6
min_size = 70
max_size = 2000
rospy.init_node('process_lidar_py')
sub = rospy.Subscriber('/outlier_removed', PointCloud2 , callback)
pub_marker = rospy.Publisher('/bounding_box_marker', Marker, queue_size=10)
txt_marker = rospy.Publisher('/text_marker', MarkerArray, queue_size=10)
client = dynamic_reconfigure.client.Client("velodyne_server", timeout=30, config_callback=parameter_callback)
rospy.spin()

