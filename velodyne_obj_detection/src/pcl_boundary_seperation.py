#!/usr/bin/env python
import rospy
import pcl
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2
import numpy as np
import ros_numpy
import pandas as pd
import math

class Boundary:
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        self.theta1 = max(math.atan2(y2, x2), math.atan2(y1, x1))
        self.theta2 = min(math.atan2(y2, x2), math.atan2(y1, x1))
        self.m = (y2-y1)/(x2-x1)
        self.c = (y1-self.m*x1)
    
    def is_inside(self, x, y):
       angle = math.atan2(y,x)
       if angle<self.theta1 and self.theta2<angle:
               if (x*self.m-y+self.c)*self.c>0:
                   return True
               else:
                   return False
       else:
            return True

def pcl_to_ros(pcl_cloud):
    ros_msg = ros_numpy.msgify(PointCloud2, np.array([(f[0], f[1], f[2], f[3]) for f in np.array(pcl_cloud)], dtype=dt))
    ros_msg.header.stamp = rospy.Time.now()
    ros_msg.header.frame_id = "velodyne"
    return ros_msg

def is_inside_all(x, y):
    #check = True
    for boundary in boundaries:
	#check = check and boundary.is_inside(x, y)
	if not boundary.is_inside(x, y):
		return False
	#if boundary.is_inside(x, y):
            #return True 	
    #if check:
	#return True
    #else:
	#return False
	
    return True
def callback(ros_cloud):
    points_list = []
    for data in pc2.read_points(ros_cloud, skip_nans=True):
        if is_inside_all(data[0], data[1]):
            points_list.append([data[0], data[1], data[2], data[3]])
    pub.publish(pcl_to_ros(points_list))
    
boundary_points = [[4,40,25,5], [25,0,1,-25]]
dt = np.dtype([('x', np.float32, 1), ('y', np.float32, 1), ('z', np.float32, 1), ('intensity', np.float32, 1)])
boundaries = [Boundary(a[0], a[1], a[2], a[3]) for a in boundary_points]
#[0,40,25,0], [25,0,0,-15] [11,10,10,-10]
rospy.init_node('remove_boundary')
sub = rospy.Subscriber('/velodyne_points', PointCloud2 , callback)
pub = rospy.Publisher('/velodyne_points_boundary_removed', PointCloud2, queue_size=10)
rospy.spin()
