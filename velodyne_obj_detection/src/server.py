#!/usr/bin/env python

import rospy

from dynamic_reconfigure.server import Server
from velodyne.cfg import velodyne_paramsConfig

def callback(config, level):
    return config

if __name__ == "__main__":
    rospy.init_node("velodyne_server", anonymous = False)

    srv = Server(velodyne_paramsConfig, callback)
    rospy.spin()
