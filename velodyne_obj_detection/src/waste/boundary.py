#!/usr/bin/env python
import math

class Boundary:
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        self.theta1 = max(math.atan2(y2, x2), math.atan2(y1, x1))
        self.theta2 = min(math.atan2(y2, x2), math.atan2(y1, x1))
        self.m = (y2-y1)/(x2-x1)
        self.c = (y1-self.m*x1)
    
    def is_inside(self, x, y):
       angle = math.atan2(y,x)
       if angle<self.theta1 and self.theta2<angle:
           if self.m > 0:
               if x*self.m-y+self.c<0:
                   return True
               else:
                   return False
           else:
               if x*self.m-y+self.c>0:
                   return True
               else:
                   return False
       else:
            return True
        

