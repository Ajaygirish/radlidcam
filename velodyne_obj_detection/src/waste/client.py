#!/usr/bin/env python

import rospy

import dynamic_reconfigure.client

def callback(config):
    print config.int_param
    #rospy.loginfo("Config set to {int_param}, {double_param}, {str_param}, {bool_param}, {size}".format(**config))

if __name__ == "__main__":
    rospy.init_node("velodyne_client")

    client = dynamic_reconfigure.client.Client("velodyne_server", timeout=30, config_callback=callback)

    r = rospy.Rate(0.1)

    while not rospy.is_shutdown():
	#print rospy.get_param("int_param")
        r.sleep()
