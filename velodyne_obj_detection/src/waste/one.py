#!/usr/bin/env python

import rospy
import pcl
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2
import numpy as np
import ros_numpy

def ros_to_pcl(ros_cloud):
    points_list = []
    for data in pc2.read_points(ros_cloud, skip_nans=True):
        points_list.append([data[0], data[1], data[2], data[3]])
    pcl_data = pcl.PointCloud_PointXYZI()
    pcl_data.from_list(points_list)
    return pcl_data
    
def pcl_to_ros(pcl_cloud):
    ros_msg = ros_numpy.msgify(PointCloud2, np.array([(f[0], f[1], f[2], f[3]) for f in np.array(pcl_cloud.to_list())], dtype=dt))
    ros_msg.header.stamp = rospy.Time.now()
    ros_msg.header.frame_id = "velodyne"
    return ros_msg
    
def voxel_filter(cloud, leaf_size = 0.1):
    vox = cloud.make_voxel_grid_filter()
    vox.set_leaf_size(leaf_size, leaf_size, leaf_size)
    return vox.filter()
    
def pass_ground_filter(cloud, zmin = -1):
    passthrough = cloud.make_passthrough_filter()
    passthrough.set_filter_field_name ('z')
    passthrough.set_filter_limits (zmin, 100)
    return passthrough.filter()

def statistical_outlier_filter(cloud, no_of_n = 50):
    fil = cloud.make_statistical_outlier_filter()
    fil.set_mean_k(no_of_n)
    fil.set_std_dev_mul_thresh(1.0)
    return fil.filter()
        
def callback(msg):
    pcl_cloud = ros_to_pcl(msg)
    voxel_cloud = voxel_filter(pcl_cloud)
    pub_vox.publish(pcl_to_ros(voxel_cloud))
    ground_filtered = pass_ground_filter(voxel_cloud)
    pub_flr.publish(pcl_to_ros(ground_filtered))
    outlier_removed = statistical_outlier_filter(ground_filtered)
    pub_olr.publish(pcl_to_ros(outlier_removed))
    
rospy.init_node('pointconvert')
dt = np.dtype([('x', np.float32, 1), ('y', np.float32, 1), ('z', np.float32, 1), ('intensity', np.float32, 1)])
rospy.Subscriber('/boundary_removed', PointCloud2 , callback)
pub_vox = rospy.Publisher('/voxel_output', PointCloud2, queue_size=10)
pub_flr = rospy.Publisher('/floor_removed', PointCloud2, queue_size=10)
pub_olr = rospy.Publisher('/outlier_removed', PointCloud2, queue_size=10)

rospy.spin()
