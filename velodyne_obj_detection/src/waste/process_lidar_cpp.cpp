#include "ros/ros.h"
#include <iostream>
#include "pcl/point_cloud.h"
#include "pcl_conversions/pcl_conversions.h"
#include "pcl/filters/voxel_grid.h"
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>

class SubscribeProcessPublish
{
public:
    SubscribeProcessPublish()
    {
        // Assign subscriber
        this->subscriber = this->nh.subscribe<pcl::PCLPointCloud2>("/velodyne_points_boundary_removed", 5, &SubscribeProcessPublish::processLidarMeasurement, this);
        
	// Assign voxel publisher
        this->voxel_publisher = this->nh.advertise<pcl::PCLPointCloud2> ("/voxel_output", 5);

	// Assign floor_removed publisher
        this->floorRemoved_publisher = this->nh.advertise<pcl::PCLPointCloud2> ("/floorRemoved_output", 5);

	// Assign outlierRemoved publisher
        this->outlierRemoved_publisher = this->nh.advertise<pcl::PCLPointCloud2> ("/outlierRemoved_output", 5);
    }
    
	void 
        processLidarMeasurement(const pcl::PCLPointCloud2ConstPtr& cloud)
    {

	// define a new container for the data
	pcl::PCLPointCloud2::Ptr cloudVoxel (new pcl::PCLPointCloud2 ());
	// define a voxelgrid
	pcl::VoxelGrid<pcl::PCLPointCloud2> voxelGrid;
	// set input to cloud
	voxelGrid.setInputCloud(cloud);
	// set the leaf size (x, y, z)
	voxelGrid.setLeafSize(0.08, 0.08, 0.08);
	// apply the filter to dereferenced cloudVoxel
	voxelGrid.filter(*cloudVoxel);

	// Publish the data for cloudVoxel
	this->voxel_publisher.publish (*cloudVoxel);

	// define a container for floorRemoved	
	pcl::PCLPointCloud2::Ptr floorRemoved (new pcl::PCLPointCloud2 ());	
	// define a PassThrough filter
	pcl::PassThrough<pcl::PCLPointCloud2> pass;
	// set input to cloudVoxel
	pass.setInputCloud(cloudVoxel);
	// filter along z-axis
	pass.setFilterFieldName("z");
	// set z-limits
	pass.setFilterLimits(-1, 100);
	// apply the filter to dereferenced floorRemoved
	pass.filter(*floorRemoved);

	// Publish the data for floorRemoved
	this->floorRemoved_publisher.publish (*floorRemoved);

	// define a container for outlierRemoved	
	pcl::PCLPointCloud2::Ptr outlierRemoved (new pcl::PCLPointCloud2 ());
  	// define a filtering object
  	pcl::StatisticalOutlierRemoval<pcl::PCLPointCloud2> sor;
	// set input to floorRemoved
  	sor.setInputCloud (floorRemoved);
	// set the number of neighbors to analyze
  	sor.setMeanK (50);
	// set standard deviation threshold
  	sor.setStddevMulThresh (1.0);
	// apply the filter to dereferenced outlierRemoved
  	sor.filter (*outlierRemoved);

	// Publish the data for outlierRemoved
	this->outlierRemoved_publisher.publish (*outlierRemoved);
    }

private:
    ros::NodeHandle nh;
    ros::Subscriber subscriber;
    ros::Publisher voxel_publisher;
    ros::Publisher floorRemoved_publisher;
    ros::Publisher outlierRemoved_publisher;
};

int main(int argc, char** argv)
{
    // initialise the node
    ros::init(argc, argv, "process_lidar_cpp");

    SubscribeProcessPublish test;
    
    // handle ROS communication events
    ros::spin();

    return 0;
}
