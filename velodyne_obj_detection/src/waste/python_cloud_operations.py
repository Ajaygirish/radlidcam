#!/usr/bin/env python

import rospy
import pcl
from sensor_msgs.msg import PointCloud2
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point
import sensor_msgs.point_cloud2 as pc2
import numpy as np
import ros_numpy
from scipy.spatial import ConvexHull
import bbox
import dynamic_reconfigure.client

class Pcl_operations:
    def __init__(self):
        self.clust_tol = 0.2
        self.min_size = 60
        self.max_size = 2000
        self.sub = rospy.Subscriber('/outlierRemoved_output', PointCloud2 , self.process_callback)
        self.pub = rospy.Publisher('/python_cloud_processed', PointCloud2, queue_size=10)
        self.pub_marker = rospy.Publisher('/bounding_box_marker', Marker, queue_size=10)
        #self.client = dynamic_reconfigure.client.Client("velodyne_server", config_callback = self.parameter_callback)
        self.lines_array = Marker()
        self.lines_array.header.frame_id = "velodyne"
        self.lines_array.id = 0 
        self.lines_array.type = self.lines_array.LINE_LIST
        self.lines_array.action = self.lines_array.ADD
        self.lines_array.scale.x = 0.02;
        self.lines_array.color.a = 1.0
        self.lines_array.color.r = 1.0
        self.lines_array.color.g = 1.0
        self.lines_array.color.b = 0.0

    def parameter_callback(self, config):
        self.clust_tol = config.cluster_tolerance
        self.min_size = config.minimum_cluster_size
        self.max_size = config.maximum_cluster_size

    def ros_to_pcl(self, ros_cloud):
        points_list = []        
        for data in pc2.read_points(ros_cloud, skip_nans=True):
            points_list.append([data[0], data[1], data[2]])
            pcl_data = pcl.PointCloud()
            pcl_data.from_list(points_list)
        return pcl_data
    
    def pcl_to_ros(self, pcl_cloud):
        ros_msg = ros_numpy.msgify(PointCloud2, np.array([(f[0], f[1], f[2], f[3]) for f in np.array(pcl_cloud.to_list())], dtype=dt))
        ros_msg.header.stamp = rospy.Time.now()
        ros_msg.header.frame_id = "velodyne"
        return ros_msg
    
    def clustering(self, pcl_cloud):
        tree = pcl_cloud.make_kdtree()
        ec = pcl_cloud.make_EuclideanClusterExtraction()
        ec.set_ClusterTolerance(self.clust_tol)
        ec.set_MinClusterSize(self.min_size)
        ec.set_MaxClusterSize(self.max_size)
        ec.set_SearchMethod(tree)
        cluster_indices = ec.Extract()
        pts = []
        for cluster in cluster_indices:
            points = [pcl_cloud[point_indices][0:3] for point_indices in cluster]
            hull_ordered = np.array([points[index] for index in ConvexHull(points).vertices])
            np.append(hull_ordered, hull_ordered[0])        
            bb = bbox.minBoundingRect(hull_ordered[:,:-1])
            m_n = min(hull_ordered[:,-1])
            m_x = max(hull_ordered[:,-1])
            pts = pts + [Point(bb[0,0], bb[0,1], m_n), Point(bb[1,0], bb[1,1], m_n),
                        Point(bb[2,0], bb[2,1], m_n), Point(bb[1,0], bb[1,1], m_n),
                        Point(bb[2,0], bb[2,1], m_n), Point(bb[3,0], bb[3,1], m_n),
                        Point(bb[0,0], bb[0,1], m_n), Point(bb[3,0], bb[3,1], m_n),
                        Point(bb[0,0], bb[0,1], m_n), Point(bb[0,0], bb[0,1], m_x),
                        Point(bb[1,0], bb[1,1], m_n), Point(bb[1,0], bb[1,1], m_x),
                        Point(bb[2,0], bb[2,1], m_n), Point(bb[2,0], bb[2,1], m_x),
                        Point(bb[3,0], bb[3,1], m_n), Point(bb[3,0], bb[3,1], m_x),
                        Point(bb[0,0], bb[0,1], m_x), Point(bb[1,0], bb[1,1], m_x),
                        Point(bb[2,0], bb[2,1], m_x), Point(bb[1,0], bb[1,1], m_x),
                        Point(bb[2,0], bb[2,1], m_x), Point(bb[3,0], bb[3,1], m_x),
                        Point(bb[0,0], bb[0,1], m_x), Point(bb[3,0], bb[3,1], m_x)]
        self.lines_array.points = pts
        self.pub_marker.publish(self.lines_array)


    
    def process_callback(self, msg):
        #pcl_cloud = 
        self.clustering(self.ros_to_pcl(msg))
        #ros_msg = pcl_to_ros(ground_filtered)
        #pub.publish(ros_msg)
    


rospy.init_node('process_lidar_py')
pop = Pcl_operations()
rospy.spin()

