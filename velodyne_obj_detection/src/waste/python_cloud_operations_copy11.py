#!/usr/bin/env python

import rospy
import pcl
from sensor_msgs.msg import PointCloud2
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Point
import sensor_msgs.point_cloud2 as pc2
import numpy as np
import ros_numpy
from scipy.spatial import ConvexHull
import bbox
import dynamic_reconfigure.client

class Properties:
    def __init__(self):
        self.mark = Marker()
        self.mark.header.frame_id = "velodyne"
        self.mark.id = 1
        self.mark.type = self.mark.LINE_LIST
        self.mark.action = self.mark.ADD
        self.mark.scale.x = 0.02;
        self.mark.color.a = 1.0
        self.mark.color.r = 1.0
        self.mark.color.g = 1.0
        self.mark.color.b = 0.0
        
def parameter_callback(config, a):
    global clust_tol, min_size, max_size
    clust_tol = config.cluster_tolerance
    min_size = config.minimum_cluster_size
    max_size = config.maximum_cluster_size

def ros_to_pcl(ros_cloud):
    points_list = []
    for data in pc2.read_points(ros_cloud, skip_nans=True):
        points_list.append([data[0], data[1], data[2]])
    pcl_data = pcl.PointCloud()
    pcl_data.from_list(points_list)
    return pcl_data
    
def pcl_to_ros(pcl_cloud):
    ros_msg = ros_numpy.msgify(PointCloud2, np.array([(f[0], f[1], f[2], f[3]) for f in np.array(pcl_cloud.to_list())], dtype=dt))
    ros_msg.header.stamp = rospy.Time.now()
    ros_msg.header.frame_id = "velodyne"
    return ros_msg
    
def clustering(pcl_cloud):
    tree = pcl_cloud.make_kdtree()
    ec = pcl_cloud.make_EuclideanClusterExtraction()
    ec.set_ClusterTolerance(clust_tol)
    ec.set_MinClusterSize(min_size)
    ec.set_MaxClusterSize(max_size)
    ec.set_SearchMethod(tree)
    cluster_indices = ec.Extract()
    lines_array = MarkerArray()
    lines_array.markers = []
    i_d = 0
    for cluster in cluster_indices:
        points = [pcl_cloud[point_indices][0:3] for point_indices in cluster]
        hull_ordered = np.array([points[index] for index in ConvexHull(points).vertices])
        np.append(hull_ordered, hull_ordered[0])        
        bb = bbox.minBoundingRect(hull_ordered[:,:-1])
        lines = Marker()
        lines.header.frame_id = "velodyne"
        lines.id = i_d 
        i_d = i_d+1
        lines.type = lines.LINE_LIST
        lines.action = lines.ADD
        m_n = min(hull_ordered[:,-1])
        m_x = max(hull_ordered[:,-1])
        lines.scale.x = 0.01;
        lines.color.a = 1.0
        lines.color.r = 1.0
        lines.color.g = 1.0
        lines.color.b = 0.0
        
        lines.points = [Point(bb[0,0], bb[0,1], m_n), Point(bb[1,0], bb[1,1], m_n),
                        Point(bb[2,0], bb[2,1], m_n), Point(bb[1,0], bb[1,1], m_n),
                        Point(bb[2,0], bb[2,1], m_n), Point(bb[3,0], bb[3,1], m_n),
                        Point(bb[0,0], bb[0,1], m_n), Point(bb[3,0], bb[3,1], m_n),
                        Point(bb[0,0], bb[0,1], m_n), Point(bb[0,0], bb[0,1], m_x),
                        Point(bb[1,0], bb[1,1], m_n), Point(bb[1,0], bb[1,1], m_x),
                        Point(bb[2,0], bb[2,1], m_n), Point(bb[2,0], bb[2,1], m_x),
                        Point(bb[3,0], bb[3,1], m_n), Point(bb[3,0], bb[3,1], m_x),
                        Point(bb[0,0], bb[0,1], m_x), Point(bb[1,0], bb[1,1], m_x),
                        Point(bb[2,0], bb[2,1], m_x), Point(bb[1,0], bb[1,1], m_x),
                        Point(bb[2,0], bb[2,1], m_x), Point(bb[3,0], bb[3,1], m_x),
                        Point(bb[0,0], bb[0,1], m_x), Point(bb[3,0], bb[3,1], m_x)]
        lines_array.markers.append(lines)
    pub_marker.publish(lines_array)


    
def callback(msg):
    pcl_cloud = ros_to_pcl(msg)
    clustering(pcl_cloud)    
    #ros_msg = pcl_to_ros(ground_filtered)
    #pub.publish(ros_msg)
    

clust_tol = 0.2
min_size = 60
max_size = 2000
rospy.init_node('process_lidar_py')
sub = rospy.Subscriber('/outlierRemoved_output', PointCloud2 , callback)
pub = rospy.Publisher('/python_cloud_processed', PointCloud2, queue_size=10)
pub_marker = rospy.Publisher('/bounding_box_marker', MarkerArray, queue_size=10)
a = 5
client = dynamic_reconfigure.client.Client("velodyne_server", timeout=30, config_callback=parameter_callback)
rospy.spin()

