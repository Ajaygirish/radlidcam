#!/usr/bin/env python
#Data_p = namedtuple('Data_p', 'x y z intensity')
#np_array = np.stack(data[f] for f in ('x', 'y', 'z', 'intensity')).T    

#np_cloud = np.array(pcl_cloud.to_list())
#cloud = np.array([(f[0], f[1], f[2], f[3]) for f in np_cloud], dtype=dt)
#msgg = ros_numpy.msgify(PointCloud2, cloud)

#msgg = ros_numpy.msgify(PointCloud2, np.array([(f[0], f[1], f[2], f[3]) for f in np.array(pcl_cloud.to_list())], dtype=dt))

    #pcl_cloud = ros_to_pcl(msg)
    #point_list = [[data[0], data[1], data[2], data[3]] for data in pc2.read_points(msg, skip_nans = True)]

    #voxel_down_pcd = o3d.voxel_down_sample(point_list,voxel_size=0.02)
    
    #msgg = ros_numpy.msgify(PointCloud2, np.array([(f[0], f[1], f[2], f[3]) for f in np.array(point_list)], dtype=dt))
    #msgg.header.stamp = rospy.Time.now()
    #msgg.header.frame_id = "velodyne"
    #dt = np.dtype([('x', np.float32, 1), ('y', np.float32, 1), ('z', np.float32, 1), ('intensity', np.float32, 1)])


from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2
import rospy
import numpy as np
from pcl_helper import *
import ros_numpy
from collections import namedtuple
import open3d
from std_msgs.msg import Header
from sensor_msgs.msg import PointCloud2, PointField
from open3d.open3d.geometry import remove_radius_outlier

FIELDS_XYZI = [
    PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
    PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
    PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1),
    PointField(name='intensity', offset=16, datatype=PointField.FLOAT32, count=1)
]

def convertCloudFromOpen3dToRos(open3d_cloud, frame_id="odom"):
    header = Header()
    header.stamp = rospy.Time.now()
    header.frame_id = frame_id
    fields=FIELDS_XYZI
    colors = np.floor(np.asarray(open3d_cloud.colors)*255)
    points=np.asarray(open3d_cloud.points)
    return pc2.create_cloud(header, fields, np.c_[points, colors[:,0]])

def convertCloudFromRosToOpen3d(ros_cloud):
    field_names=[field.name for field in ros_cloud.fields]
    cloud_data = list(pc2.read_points(ros_cloud, skip_nans=True, field_names = field_names))
    xyz = [(x,y,z) for x,y,z,i,r in cloud_data]
    iii = [(i,i,i) for x,y,z,i,r in cloud_data]
    open3d_cloud = open3d.PointCloud()
    open3d_cloud.points = open3d.Vector3dVector(np.array(xyz))
    open3d_cloud.colors = open3d.Vector3dVector(np.array(iii)/255.0)
    return open3d_cloud

pub = rospy.Publisher('/post_processed', PointCloud2, queue_size=10)

def callback(msg):
    o3d_data = convertCloudFromRosToOpen3d(msg)
    voxel_down_pcd = open3d.voxel_down_sample(o3d_data,voxel_size=0.05)
    
    edited, ind = remove_radius_outlier(voxel_down_pcd,nb_points=16, radius=0.05)
    msgg = convertCloudFromOpen3dToRos(edited, "velodyne")
    pub.publish(msgg)

rospy.init_node('pointconvert')
sub = rospy.Subscriber('/velodyne_points', PointCloud2 , callback)
rospy.spin()